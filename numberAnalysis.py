# Name: Joey Carberry
# Date: October 1, 2015
# Project: 4 Number Analysis

# Finds the smallest number in a list
userList = []
def minList():
    print ('Minimum value: ' , min(userList))
def maxList():
    print ('Maximum value: ' , max(userList))
def sumList():
    print ('Sum: ' , sum(userList))
def averageList():
    average = sum(userList) / len(userList)
    print('Average value: ' , average)

for x in range(0,20):
    userValue = int(input('Enter an integer: '))
    userList.append(userValue)

print (userList)
minList()
maxList()
sumList()
averageList()

