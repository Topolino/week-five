#Name: Joey Carberry
#Date: September 29, 2015
#Project: Project Three "Prime Numbers"
'''
runValue = True
while runValue:
    numInput = int(input('Input a number: '))

    def is_prime(numInput):
        for x in range(2,numInput):
            if(numInput % x == 0):
                return False
            else:
                return True

    if(is_prime(numInput) == True):
        print('This is a prime number')
    else:
        print('This is not a prime number')
'''
# Random Ints between 1 - 100 Diplayed
# --------------------------------------

runValue = True
while runValue:
    for numInput in range(2,101):

        def is_prime(numInput):
            for x in range(2,numInput):
                if(numInput % x == 0):
                    return False
                else:
                    return True

        if(is_prime(numInput) == True):
            print(str(numInput), ' is a prime number')
        if (numInput == 101):
            runValue = False
        else:
            print(str(numInput), ' is not a prime number')
