#Name: Joey Carberry
#Date: September 29, 2015
#Project: BonusProject 'Rock Paper Scissors'

import random
continueRun = True
while continueRun:
    compChoice = random.randint(1,4)
    print ('1 - Rock')
    print ('2 - Paper')
    print ('3 - Scissors')
    userChoice = int(input('Your choice: '))

    def winMessage():
        print ('You win')
        print ('You played ', userChoice, 'The computer played ', compChoice)
    def lossMessage():
        print ('You lose')
        print ('You played ', userChoice, 'The computer played ', compChoice)

    if (compChoice == userChoice):
        print ('Tied')
        print ('You played ', userChoice, 'The computer played ', compChoice)

    if (compChoice  ==  1 and userChoice == 2):
        winMessage()
    if (compChoice == 1 and userChoice == 3):
        lossMessage()
    if (compChoice == 2 and userChoice == 1):
        lossMessage()
    if (compChoice == 2 and userChoice == 3):
        winMessage()
    if (compChoice == 3 and userChoice == 1):
        winMessage()
    if (compChoice == 3 and userChoice == 2):
        lossMessage()

    continuePrompt = input('Continue Y/N? ')
    if (continuePrompt == 'N' or continuePrompt == 'n'):
        continueRun = False